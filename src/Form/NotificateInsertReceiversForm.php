<?php

namespace Drupal\notificate_insert\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Site\Settings;

/**
 * Class NotificateInsertReceiversForm.
 */
class NotificateInsertReceiversForm extends ConfigFormBase {

  /**
   * Drupal\Core\Site\Settings definition.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;
  /**
   * Constructs a new NotificateInsertReceiversForm object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
      Settings $settings
    ) {
    parent::__construct($config_factory);
        $this->settings = $settings;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
            $container->get('settings')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'notificate_insert.notificateinsertreceivers',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'notificate_insert_receivers_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('notificate_insert.notificateinsertreceivers');
    $form['pending'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only notify comments pending publication'),
      '#description' => $this->t('Pending publication'),
      '#default_value' => $config->get('pending'),
    ];
    $form['notify_site_mail'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Notify site mail'),
      '#description' => $this->t('Send a mail to site&#039;s mail (currently %mail)', ['%mail' => $this->config('system.site')->get('mail')]),
      '#default_value' => $config->get('notify_site_mail'),
    ];
    $form['roles'] = [
      '#type' => 'select',
      '#title' => $this->t('roles'),
      '#description' => $this->t('The roles to notify'),
      '#options' => user_role_names(True),//['role1' => $this->t('role1'), 'role2' => $this->t('role2')],
      '#size' => 5,
      '#default_value' => $config->get('roles'),
      '#required' => False,
      '#multiple' => True,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('notificate_insert.notificateinsertreceivers')
      ->set('notify_site_mail', $form_state->getValue('notify_site_mail'))
      ->set('roles', $form_state->getValue('roles'))
      ->set('pending', $form_state->getValue('pending'))
      ->save();
  }

}
